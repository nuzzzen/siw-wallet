<h1 align="center">Siwallet</h1>

<div align="center">
  <img src="src/main/resources/static/images/png/logo-color.png" alt="siw-wallet-logo" width="200" height="200">
</div>

<div align="center">
</div>
<div align='center'>
    <img alt="GitLab last commit" src="https://img.shields.io/badge/SpringBoot-6DB33F?style=for-the-badge&logo=Spring&logoColor=white&label=3.0.6
">
</div>

## Indice

- [Descrizione](#descrizione)
- [Funzionalità Principali](#funzionalità-principali)
- [Known Bugs](#known-bugs)
- [Tecnologie Utilizzate](#tecnologie-utilizzate)
  - [Pipeline CI/CD](#pipeline-cicd)

## Descrizione

Siwallet è un'applicazione web per la gestione delle finanze personali. L'app consente agli utenti di registrare spese,
entrate e visualizzare statistiche di base sulla propria situazione finanziaria.

## Funzionalità Principali

- **Creazione e Modifica del Wallet**: Ogni utente possiede un wallet personale, che può essere creato e modificato,
  ma non cancellato. Ogni wallet ha un nome, una descrizione, un saldo iniziale (zero) e una valuta.
- **Registrazione delle Spese/Entrate**: Gli utenti possono inserire spese effettuate od entrate, inclusi importo, data
  e descrizione,
  stato, sender e receiver.
- **Cancellazione Spese/Entrate**: Gli utenti possono cancellare le spese/entrate registrate nel proprio wallet.
- **Visualizzazione delle Transazioni**: Gli utenti possono visualizzare un elenco delle transazioni registrate
  ed eventualmente modificare.
- **Statistiche di Base**: Si fornisce un riepilogo del saldo attuale del wallet.

## Known Bugs

- [ ] Funzionalità delle categorie non implementata completamente. Presenti errori nella lettura delle
  categorie di un wallet con successiva assegnazione alla transazione.

## Tecnologie Utilizzate

- **Java 17 con Spring Boot 6.0.3** per il backend.
- **Bootstrap (HTML, CSS)** e **Thymeleaf** per l'interfaccia utente.
- **DBMS MariaDB** per la persistenza dei dati.

### Pipeline CI/CD

È stata integrata anche la CI/CD con _Gitlab CI_, la quale permette di buildare il progetto e, in caso di
creazione di un TAG e _build_ andati a buon fine, di deployare il progetto su AWS tramite beanstalk.
L'URL dell'applicazione è: http://siwallet.eu-central-1.elasticbeanstalk.com/

