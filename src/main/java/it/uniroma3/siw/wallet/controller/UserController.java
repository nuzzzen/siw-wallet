package it.uniroma3.siw.wallet.controller;

import it.uniroma3.siw.wallet.model.User;
import it.uniroma3.siw.wallet.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequiredArgsConstructor
@Log4j2
public class UserController {
    private final UserService userService;

    @RequestMapping("/user")
    public String getUser(Model model, Authentication authentication) {
        User user = getCurrentUser(authentication);
        if (user != null) {
            model.addAttribute("user", userService.getUserByUsername(user.getUsername()));
            return "user";
        }
        return "redirect:/login";
    }

    private User getCurrentUser(Authentication authentication) {
        if (authentication != null &&
                authentication.isAuthenticated() &&
                userService.existsUserByUsername(authentication.getName())) {
            return userService.getUserByUsername(authentication.getName());
        }
        return null;
    }

}
