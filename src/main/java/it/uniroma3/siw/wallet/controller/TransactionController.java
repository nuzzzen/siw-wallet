package it.uniroma3.siw.wallet.controller;

import it.uniroma3.siw.wallet.enumeration.transactions.TransactionStatus;
import it.uniroma3.siw.wallet.model.Transaction;
import it.uniroma3.siw.wallet.model.User;
import it.uniroma3.siw.wallet.service.TransactionService;
import it.uniroma3.siw.wallet.service.UserService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.UUID;

@Controller
@RequestMapping("/wallet/")
@RequiredArgsConstructor
@Log4j2
public class TransactionController {

    private final TransactionService transactionService;
    private final UserService userService;

    @GetMapping("/addTransaction")
    public String addTransactionForm(Model model, TransactionStatus transactionStatus) {
        User user = userService.getUserByUsername(GlobalController.getUser().getUsername());
        log.info("Got userdetails: " + user.getUsername());


        model.addAttribute("transaction", new Transaction());
        model.addAttribute("wallet", user.getWallet());
        model.addAttribute("status", TransactionStatus.values());
        return "addTransactionForm";
    }

    @PostMapping("/addTransaction")
    public String addTransaction(@Valid @ModelAttribute("transaction")
                                     Transaction transaction,
                                 BindingResult bindingResult) {

        User user = userService.getUserByUsername(GlobalController.getUser().getUsername());

        if (transaction.getAmount().compareTo(BigDecimal.ZERO) == 0) {
            log.info("amount is zero");
            bindingResult.rejectValue("amount", "negative.amount", "Amount cannot be zero.");
            return "addTransactionForm";
        } else if (transaction.getStatus() == null) {
            log.info("status is null");
            bindingResult.rejectValue("status", "invalid.status", "Status is invalid.");
            return "addTransactionForm";
        }

        transaction.setWallet(user.getWallet());
        transaction.setCurrency(user.getWallet().getCurrency());
        transaction.setCreatedAt(Instant.now());

        transactionService.createTransaction(transaction);
        log.info("transaction saved to db");

        return "redirect:/wallet/" + user.getWallet().getUuid();
    }

    @GetMapping("/editTransaction/{uuid}")
    public String editTransactionForm(@PathVariable("uuid") UUID uuid,
                                      Model model) {
        Transaction transaction = transactionService.getTransactionByUuid(uuid);
        if (transaction == null) {
            model.addAttribute("error", "Transaction not found.");
            return "redirect:/wallet/";
        }
        model.addAttribute("transaction", transaction);
        model.addAttribute("wallet", transaction.getWallet());
        model.addAttribute("status", TransactionStatus.values());
        return "editTransactionForm";
    }

    @PostMapping("/editTransaction/{uuid}")
    public String editTransaction(@PathVariable("uuid") UUID uuid,
                                  @Valid @ModelAttribute("transaction") Transaction transaction,
                                  BindingResult bindingResult,
                                  Model model) {
        if (bindingResult.hasErrors() || transactionService.getTransactionByUuid(uuid) == null) {
            model.addAttribute("error", "Transaction not found.");
            return "redirect:/wallet/";
        }
        if (transaction.getAmount().compareTo(BigDecimal.ZERO) == 0) {
            bindingResult.rejectValue("amount", "negative.amount", "Amount cannot be zero.");
            return "editTransactionForm";
        }

        transactionService.getTransactionByUuid(uuid).setAmount(transaction.getAmount());
        transactionService.getTransactionByUuid(uuid).setDescription(transaction.getDescription());
        transactionService.getTransactionByUuid(uuid).setSender(transaction.getSender());
        transactionService.getTransactionByUuid(uuid).setReceiver(transaction.getReceiver());
        transactionService.getTransactionByUuid(uuid).setStatus(transaction.getStatus());

        transactionService.updateTransaction(transaction);
        return "redirect:/wallet/" + transactionService.getTransactionByUuid(uuid).getWallet().getUuid();
    }

    @PostMapping("/deleteTransaction/{uuid}")
    public String deleteTransaction(@PathVariable("uuid") UUID uuid,
                                    @Valid @ModelAttribute("transaction") Transaction transaction,
                                    BindingResult bindingResult,
                                    Model model) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("error", "Transaction not found.");
            return "redirect:/wallet/";
        }
        if (transactionService.getTransactionByUuid(uuid) == null) {
            model.addAttribute("error", "Transaction not found.");
            return "redirect:/wallet/";
        }
        UUID walletUuid = transactionService.getTransactionByUuid(uuid).getWallet().getUuid();
        transactionService.deleteTransaction(uuid);

        return "redirect:/wallet/" + walletUuid;
    }
}
