package it.uniroma3.siw.wallet.controller;

import it.uniroma3.siw.wallet.model.User;
import it.uniroma3.siw.wallet.model.Wallet;
import it.uniroma3.siw.wallet.service.TransactionService;
import it.uniroma3.siw.wallet.service.UserService;
import it.uniroma3.siw.wallet.service.WalletService;
import it.uniroma3.siw.wallet.utils.WalletBalanceCalculator;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import java.math.BigDecimal;
import java.util.UUID;

@Controller
@RequiredArgsConstructor
@Log4j2
public class WalletController {

    private final WalletService walletService;
    private final TransactionService transactionService;
    private final UserService userService;


    @GetMapping({"/wallet", "/wallet/{uuid}"})
    public String getWallet(@PathVariable(required = false) UUID uuid,
                            Model model,
                            Authentication authentication) {
        User user = getCurrentUser(authentication);

        if (user != null) {
            if (uuid != null) {
                Wallet wallet = walletService.getWalletByUuid(uuid);
                if (wallet != null && user.getUsername().equals(wallet.getUser().getUsername())) {
                    WalletBalanceCalculator.calculateBalance(wallet);
                    model.addAttribute("wallet", wallet);
                    model.addAttribute("transactions", transactionService.getAllTransactionsByWallet(wallet));
                    return "wallet";
                } else {
                    return "redirect:/login";
                }
            }

            if (user.getWallet() != null) {
                return "redirect:/wallet/" + user.getWallet().getUuid();
            }
        }

        return "redirect:/login";
    }

    @GetMapping("/wallet/create")
    public String createWalletForm(Model model) {
        model.addAttribute("wallet", new Wallet());
        return "createWalletForm";
    }

    @PostMapping("/wallet/create")
    public String createWallet(@ModelAttribute("wallet") @Valid Wallet wallet, Authentication authentication, BindingResult bindingResult, Model model) {
        User user = userService.getUserByUsername(authentication.getName());

        if (bindingResult.hasErrors()) {
            model.addAttribute("error", true);
            return "createWalletForm";
        }
        wallet.setUser(user);
        wallet.setBalance(new BigDecimal(0));
        walletService.createWallet(wallet);
        return "redirect:/wallet/" + wallet.getUuid();
    }

    private User getCurrentUser(Authentication authentication) {
        if (authentication != null &&
                authentication.isAuthenticated() &&
                userService.existsUserByUsername(authentication.getName())) {
            return userService.getUserByUsername(authentication.getName());
        }
        return null;
    }

    @GetMapping("/wallet/edit/{uuid}")
    public String editWalletForm(@PathVariable UUID uuid, Model model, Authentication authentication) {
        User user = getCurrentUser(authentication);
        Wallet wallet = walletService.getWalletByUuid(uuid);

        if (user != null && wallet != null) {
            log.info("Obtained user: %s and wallet %s".formatted(user.getUsername(), wallet.getName()));
            if (user.getUsername().equals(wallet.getUser().getUsername())) {
                model.addAttribute("wallet", wallet);
                return "editWalletForm";
            }
        }
        return "redirect:/login";
    }

    @PostMapping("/wallet/edit/{uuid}")
    public String editWallet(@PathVariable UUID uuid,
                             @ModelAttribute("wallet") @Valid Wallet wallet,
                             Authentication authentication,
                             BindingResult bindingResult,
                             Model model) {

        User user = getCurrentUser(authentication);
        Wallet oldWallet = walletService.getWalletByUuid(uuid);

        if (bindingResult.hasErrors()) {
            model.addAttribute("error", true);
            return "editWalletForm";
        }
        if (user != null && oldWallet != null) {
            log.info("Obtained user: %s and wallet %s".formatted(user.getUsername(), oldWallet.getName()));
            if (user.getUsername().equals(oldWallet.getUser().getUsername())) {
                oldWallet.setName(wallet.getName());
                oldWallet.setDescription(wallet.getDescription());
                oldWallet.setCurrency(wallet.getCurrency());
                walletService.updateWallet(oldWallet);
                return "redirect:/wallet/" + oldWallet.getUuid();
            }
        }
        return "redirect:/login";
    }

}
