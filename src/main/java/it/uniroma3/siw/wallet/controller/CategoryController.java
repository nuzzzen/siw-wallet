package it.uniroma3.siw.wallet.controller;


import it.uniroma3.siw.wallet.model.Category;
import it.uniroma3.siw.wallet.model.User;
import it.uniroma3.siw.wallet.service.CategoryService;
import it.uniroma3.siw.wallet.service.UserService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/wallet/")
@RequiredArgsConstructor
@Log4j2
public class CategoryController {

    private final CategoryService categoryService;
    private final UserService userService;

    @GetMapping("/addCategory")
    public String addCategoryForm(Model model) {
        model.addAttribute("category", new Category());
        return "addCategoryForm";
    }

    @PostMapping("/addCategory")
    public String addCategory(@Valid @ModelAttribute("category") Category category, Model model) {
        if (category.getName().isEmpty()) {
            model.addAttribute("error", "Name cannot be empty.");
            return "addCategoryForm";
        }
        User user = userService.getUserByUsername(GlobalController.getUser().getUsername());
        log.info("Got userdetails: " + user.getUsername());
        category.setWallet(user.getWallet());
        categoryService.createCategory(category);
        return "redirect:/wallet/" + user.getWallet().getUuid();
    }

    @PostMapping("/deleteCategory")
    public String deleteCategory(@Valid @ModelAttribute("category") Category category, Model model) {
        if (category == null) {
            model.addAttribute("error", "Category not found.");
            return "redirect:/wallet/";
        }
        categoryService.deleteCategory(category.getUuid());
        return "redirect:/wallet/";
    }

}
