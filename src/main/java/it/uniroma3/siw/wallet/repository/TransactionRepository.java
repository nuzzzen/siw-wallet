package it.uniroma3.siw.wallet.repository;

import it.uniroma3.siw.wallet.model.Transaction;
import it.uniroma3.siw.wallet.model.Wallet;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface TransactionRepository extends JpaRepository<Transaction, UUID> {
    Optional<Transaction> findByUuid(UUID uuid);
    List<Transaction> findAllByWallet(Wallet wallet);
}