package it.uniroma3.siw.wallet.repository;

import it.uniroma3.siw.wallet.model.Wallet;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.UUID;

public interface WalletRepository extends JpaRepository<Wallet, UUID> {

    Optional<Wallet> findWalletByUuid(UUID walletUuid);
}