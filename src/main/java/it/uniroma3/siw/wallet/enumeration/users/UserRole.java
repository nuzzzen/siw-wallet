package it.uniroma3.siw.wallet.enumeration.users;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.GrantedAuthority;

@Getter
@RequiredArgsConstructor
public enum UserRole implements GrantedAuthority {
    DEFAULT_ROLE("DEFAULT"),
    //TODO    PREMIUM_ROLE("DEFAULT"),
    ADMIN_ROLE("ADMIN");

    private final String key;

    @Override
    public String getAuthority() {
        return name();
    }
}
