package it.uniroma3.siw.wallet.enumeration.transactions;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum TransactionStatus {
    PENDING,
    COMPLETED,
    FAILED
}
