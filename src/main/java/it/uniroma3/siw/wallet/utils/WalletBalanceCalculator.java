package it.uniroma3.siw.wallet.utils;

import it.uniroma3.siw.wallet.enumeration.transactions.TransactionStatus;
import it.uniroma3.siw.wallet.model.Transaction;
import it.uniroma3.siw.wallet.model.Wallet;
import lombok.experimental.UtilityClass;

import java.math.BigDecimal;
import java.util.List;

@UtilityClass
public class WalletBalanceCalculator {

    /**
     * Calculate the balance of a wallet based on its transactions.
     *
     * @param wallet The wallet for which to calculate the balance.
     */
    public static void calculateBalance(Wallet wallet) {
        BigDecimal balance = wallet.getBalance();
        List<Transaction> transactions = wallet.getTransactionList();

        for (Transaction transaction : transactions) {
            if (transaction.getStatus() == TransactionStatus.COMPLETED) {
                balance = balance.add(transaction.getAmount());
            }
        }
        wallet.setBalance(balance);
    }
}
