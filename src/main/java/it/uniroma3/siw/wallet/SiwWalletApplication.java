package it.uniroma3.siw.wallet;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SiwWalletApplication {

	public static void main(String[] args) {
		SpringApplication.run(SiwWalletApplication.class, args);
	}

}
