package it.uniroma3.siw.wallet.service.impl;

import it.uniroma3.siw.wallet.model.Wallet;
import it.uniroma3.siw.wallet.repository.WalletRepository;
import it.uniroma3.siw.wallet.service.WalletService;
import jakarta.persistence.EntityNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
@RequiredArgsConstructor
public class WalletServiceImpl implements WalletService {

    private final WalletRepository walletRepository;

    @Override
    public Wallet getWalletByUuid(UUID walletUuid) {
        return walletRepository
                .findWalletByUuid(walletUuid)
                .orElseThrow(() -> new EntityNotFoundException(
                        "Cannot find wallet with ID %s".formatted(walletUuid))
                );
    }

    @Override
    public void createWallet(Wallet wallet) {
        walletRepository.save(wallet);
    }

    @Override
    public void updateWallet(Wallet wallet) {
        Wallet walletToUpdate = walletRepository
                .findWalletByUuid(wallet.getUuid())
                .orElseThrow(() -> new EntityNotFoundException(
                        "Cannot find wallet with ID %s".formatted(wallet.getUuid())));
        walletRepository.save(walletToUpdate);
    }
}
