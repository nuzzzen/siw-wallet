package it.uniroma3.siw.wallet.service;

import it.uniroma3.siw.wallet.model.Category;

import java.util.List;
import java.util.UUID;

public interface CategoryService {

    Category getCategoryByName(String categoryName);

    void createCategory(Category category);

    void deleteCategory(UUID categoryId);

    List<Category> getAllCategories();
}
