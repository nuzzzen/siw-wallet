package it.uniroma3.siw.wallet.service;

import it.uniroma3.siw.wallet.model.Wallet;

import java.util.UUID;

public interface WalletService {

    Wallet getWalletByUuid(UUID walletUuid);

    void createWallet(Wallet wallet);

    void updateWallet(Wallet wallet);

}
