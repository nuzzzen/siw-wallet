package it.uniroma3.siw.wallet.service;

import it.uniroma3.siw.wallet.model.Transaction;
import it.uniroma3.siw.wallet.model.Wallet;

import java.util.List;
import java.util.UUID;

public interface TransactionService {

    Transaction getTransactionByUuid(UUID transactionUuid);

    List<Transaction> getAllTransactionsByWallet(Wallet wallet);

    void createTransaction(Transaction transaction);

    void deleteTransaction(UUID transactionUuid);

    void updateTransaction(Transaction transaction);
}
