package it.uniroma3.siw.wallet.service.impl;

import it.uniroma3.siw.wallet.model.User;
import it.uniroma3.siw.wallet.repository.UserRepository;
import it.uniroma3.siw.wallet.service.UserService;
import jakarta.persistence.EntityNotFoundException;
import lombok.AllArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import static it.uniroma3.siw.wallet.enumeration.users.UserRole.DEFAULT_ROLE;

@Service
@AllArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    @Override
    public Boolean existsUserByEmail(String email) {
        return userRepository.existsByEmail(email);
    }

    @Override
    public Boolean existsUserByUsername(String username) {
        return userRepository.existsByUsername(username);
    }

    @Override
    public User getUserByUsername(String username) {
        return userRepository
                .findUserByUsername(username)
                .orElseThrow(() -> new EntityNotFoundException(
                        "Cannot find any user associated to %s username.".formatted(username))
                );
    }

    @Override
    public void saveUser(User user) {

        if (user.getUserRole() == null) {
            user.setUserRole(DEFAULT_ROLE);
        }
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        userRepository.save(user);
    }
}
