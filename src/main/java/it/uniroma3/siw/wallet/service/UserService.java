package it.uniroma3.siw.wallet.service;

import it.uniroma3.siw.wallet.model.User;


public interface UserService {

    Boolean existsUserByEmail(String email);

    Boolean existsUserByUsername(String username);

    User getUserByUsername(String username);

    void saveUser(User user);

}
