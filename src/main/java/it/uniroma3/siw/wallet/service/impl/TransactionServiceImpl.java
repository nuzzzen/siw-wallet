package it.uniroma3.siw.wallet.service.impl;

import it.uniroma3.siw.wallet.model.Transaction;
import it.uniroma3.siw.wallet.model.Wallet;
import it.uniroma3.siw.wallet.repository.TransactionRepository;
import it.uniroma3.siw.wallet.service.TransactionService;
import jakarta.persistence.EntityNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class TransactionServiceImpl implements TransactionService {

    private final TransactionRepository transactionRepository;

    @Override
    public void createTransaction(Transaction transaction) {
        transactionRepository.save(transaction);
    }

    @Override
    public Transaction getTransactionByUuid(UUID transactionUuid) {
        return transactionRepository.findByUuid(transactionUuid).orElseThrow(
                () -> new EntityNotFoundException("Transaction not found.")
        );
    }

    @Override
    public void deleteTransaction(UUID transactionUuid) {
        transactionRepository.deleteById(transactionUuid);
    }

    @Override
    public void updateTransaction(Transaction transaction) {
        Transaction transactionToUpdate = transactionRepository.findByUuid(
                        transaction.getUuid())
                .orElseThrow(() -> new EntityNotFoundException(
                        "Cannot find transaction with ID %s".formatted(transaction.getUuid())));
        transactionRepository.save(transactionToUpdate);
    }

    @Override
    public List<Transaction> getAllTransactionsByWallet(Wallet wallet) {

        return transactionRepository.findAllByWallet(wallet);
    }
}
