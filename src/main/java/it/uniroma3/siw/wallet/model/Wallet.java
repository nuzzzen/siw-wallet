package it.uniroma3.siw.wallet.model;

import jakarta.persistence.*;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Currency;
import java.util.List;
import java.util.UUID;

@Data
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Wallet {
    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    private UUID uuid;

    @Column(nullable = false, length = 30)
    @Size(max = 30)
    private String name;

    @Column(nullable = false, length = 100)
    @Size(max = 100)
    private String description;

    @Column(nullable = false)
    private Currency currency;

    @Column(nullable = false)
    private BigDecimal balance;

    @OneToMany(mappedBy = "wallet")
    private List<Transaction> transactionList;

    @OneToOne
    private User user;

    @OneToMany(mappedBy = "wallet")
    private List<Category> categoryList;
}
