package it.uniroma3.siw.wallet.model;

import it.uniroma3.siw.wallet.enumeration.transactions.TransactionStatus;
import jakarta.persistence.*;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.Currency;
import java.util.UUID;

@Data
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Transaction {

    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    private UUID uuid;

    @Column(nullable = false)
    private BigDecimal amount;

    @Column(nullable = false)
    private Currency currency;

    @Column(length = 100)
    @Size(max = 100)
    private String description;

    @Column(length = 30)
    @Size(max = 30)
    private String sender;

    @Column(length = 30)
    @Size(max = 30)
    private String receiver;

    @CreationTimestamp
    private Instant createdAt;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false, length = 10)
    private TransactionStatus status;

    @ManyToOne
    private Category category;

    @ManyToOne
    private Wallet wallet;
}
